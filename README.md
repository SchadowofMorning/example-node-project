# Example Node Application

## Installation

    - download & install: https://nodejs.org/en/

    - download project  

    - prüft ob node installiert ist indem ihr "node -v" und "npm -v" als cmd ausführt. Beide commands sollten kein fehler ausgeben

    - geht in /app und führt das command "npm run setup" aus
    - dann startet das command "npm start"

    - nun sollte die website unter http://localhost:3000 erreichbar sein


## Pfade

Alle HTML documente findet ihr unter /app/View

Die Anwendung ist geschrieben in NodeJS. wer keine kenntnisse davon halt sollte nur sich die Ordner /View und /public anschauen.
