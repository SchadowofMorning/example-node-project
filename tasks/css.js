const gulp = require('gulp')
const sass = require('gulp-ruby-sass')
const cleanCSS = require('gulp-clean-css')

module.exports = (src, target) => {
  sass(src)
  .on('error', sass.logError)
  .pipe(cleanCSS())
  .pipe(gulp.dest(target))
}
