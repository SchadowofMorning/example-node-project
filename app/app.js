const express = require('express') //importierung des webframeworks express
const app = express() //initialisierung express
const ctrl = require('./Controller/index.js') //import des controllers
const router = require('./Router/index.js') //import des routers
const passport = require("passport")
global.viewroot = __dirname + "/View/"; //setzen einer globalen variable
app.use(express.static('public')) //statische bereitstellung aller dateien in 'public'
app.use("/", router.root)
app.listen(3000, function(){ // express wird angewiesen auf dem PORT 3000 zu reagieren
  console.log("Webpage runs on: 3000") // ausgabe an die console
})
