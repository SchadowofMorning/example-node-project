
const express = require('express') //import express
var router = express.Router() //erstellung eines dynamischen routenhandlers

router.get('/', function(req, res){ // Behandle alle "GET"-Anfragen auf der Route "/"
  res.sendFile(global.viewroot + "home.html") // sende die datei home.html
})
module.exports = router; //exportiere den routenhandler
