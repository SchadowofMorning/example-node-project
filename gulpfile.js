const gulp = require('gulp')
const css = require('./tasks/css')
const css_src = "./src/css/*.scss"
const css_target = "./app/public/css/"
gulp.task('css', css(css_src, css_target))
